/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora_isaaccabriadiez;

import java.util.Scanner;

/**
 *
 * @author DAM103
 */
public class Calculadora_IsaacCabriaDiez {

    static Scanner scanner = new Scanner(System.in);
    static int opcion = -1;
    static double numero1 = 0, numero2=0;
    public static void main(String[] args) {
        while(opcion !=0){
            try{
                System.out.println("Elige una opcion: \n"+""
                + "1.- Sumar\n"
                + "2.- Restar\n"
                + "3.- Multiplicar\n"
                + "4.- Dividir\n"
                + "5.- Porcentajes\n"
                + "0.- Salir\n");
                
                System.out.println("Selecciona una opcion de 0 a 5");
                opcion = Integer.parseInt(scanner.nextLine());
                
                switch(opcion){
                    case 1:
                        pideNumeros();
                        System.out.println(numero1+" + "+numero2+" = %,.2f"+ (numero1+numero2));
                        break;
                    case 2:
                        pideNumeros();
                        System.out.println(numero1+" - "+numero2+" = %,.2f"+ (numero1-numero2));
                        break;
                    case 3:
                        pideNumeros();
                        System.out.println(numero1+" * "+numero2+" = %,.2f"+ (numero1*numero2));
                        break;
                        
                    case 4:
                        pideNumeros();
                        System.out.println(numero1+" / "+numero2+" = %,.2f"+ (numero1/numero2));
                        break;
                    case 5:
                        pideNumeros();
                        System.out.println(numero1+" % "+numero2+" = %,.2f"+ (numero1*numero2)/100);
                        break;
                    case 0:
                        System.out.println("Saliendo...");
                        break;
                    default:
                        System.out.println("Opcion no disponible");
                        break;
                }
                System.out.println("\n");
            }catch (Exception e){
                System.out.println("Error!");
            }
        }
    }
    public static void pideNumeros(){
        System.out.println("Introduce el primer numero");
        numero1 = Integer.parseInt(scanner.nextLine());
        
        System.out.println("Introduce el primer numero");
        numero2 = Integer.parseInt(scanner.nextLine());
    }
    
}
